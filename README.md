![输入图片说明](%E4%BA%91%E7%AB%AF%E6%96%B9%E8%88%9Flogo_%E7%94%BB%E6%9D%BF%201.png)
# 项目概况
&emsp;&emsp;云端方舟控股通过搭建民间应急互助救援平台，成为官方救援的有益补充，以民间力量协助政府救援，通过覆盖救援盲区、提升救援效率、促进危机互助、常态化平台运营等方式，助益政府应急管理体系建设，提升民间危机互助救援水平。同时公司拓展应急包、应急设施、应急灾害保险、应急安全培训等业务板块，既具公益属性，又具市场空间。
# 技术栈概览
## 1.1 软件工程技术汇总
![输入图片说明](https://images.gitee.com/uploads/images/2022/0208/185425_70817ba4_7874429.png "图片1.png")
## 1.2 后端开发
### 1.2.1 Flask后台框架
&emsp;&emsp;本项目后端采用Flask开发，用于处理从前端获取的数据，Flask具有快速模板，强大的WSGI功能，在Web应用程序和库级别的完整单元可测性。
### 1.2.2 数据存储
&emsp;&emsp;后台服务器中的数据存储在MySQL数据库中，支持多种格式输入，通过编写 Python 脚本实现对Excel中的内容自动化输入到MySQL数据库里面，自动化程度高。
### 1.2.3 算法合集：CloudArk核心算法库
![输入图片说明](https://images.gitee.com/uploads/images/2022/0208/224534_d12cbc09_7874429.png "ce9c1ab13faadcd531f1c3e2a2eebd0.png")
## 1.3 前端开发
### 1.3.1 Vue.js渐进式前端框架
&emsp;&emsp;APP前端采用Vue框架，vue.js最大的优点是通过MVVM思想实现数据的双向绑定，让CloudArk开发者有更多的时间去思考业务逻辑，真正实现敏捷开发。
### 1.3.1 丰富的前端组件库
&emsp;&emsp;我们通过预先设计CloudArk小组件，把一个单页应用中的各种模块拆分到一个一个单独的组件中，后续开发只要先在父级应用中写好各种组件标签，并且在组件标签中写好要传入组件的参数，然后再分别写好各种组件的实现，实现整个应用的搭建。
### 1.4 项目部署
&emsp;&emsp;服务器配置与Kubernetes部署见附件。
### 1.5 项目开源
&emsp;&emsp;为了更好的优化CloudArk算法库、呼吁更多技术力量加入我们项目以及完善互助救援生态，本项目前端与后端部分代码分别在Github和Gitee上开源，项目地址及二维码如下：
# 2 核心业务逻辑
## 2.1 平台用户交互关系
![输入图片说明](https://images.gitee.com/uploads/images/2022/0208/223008_2048da5e_7874429.png "图片2.png")
# 服务器配置
## 一、管理电脑（Windows 10）
#### （一）数据库
* MySQL命令行：Version 5.7.26.0<br>
账户：root<br>密码：root 
* MySQL图形化界面：Version 6.3.5
## 二、服务器端（Raspberry Pi 4B | Raspbian系统）
#### （一）MariaDB（其他数据库如MySQL、Oracle也可以）
    sudo apt-get update 
    sudo apt upgrade
    sudo apt-get install mariadb-server //安装
    sudo mysql_secure_installation //设置安全，有的人没有设置这个也能连上
    sudo mysql -u root -p //检查是否成功安装
    GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'root' WITH GRANT OPTION;
    FLUSH PREVILEGE;
    sudo vim /etc/mysql/mariadb.conf.d/50-server.cnf //注释掉 bind-address = 127.0.0.1
    service mariadb status
#### （二）Node.js
    sudo apt install nodejs //下载node.js
    npm install -g enhancer-data-bridge //下载桥（没有梯子的话最好自己选个国内的源）
    data-bridge start --port 5312 //启动桥
#### （三）Redis
    wget http://download.redis.io/releases/redis-4.0.8.tar.gz
    tar -xzf redis-4.0.8.tar.gz //解压
    cd redis-4.0.8 & make //进入解压根目录并且编译
    ./src/redis-server --port 57192 //启动 Redis 服务
<div STYLE="page-break-after: always;"></div>

## 三、部署
```
tar -zxf bodhi-app-[发布编号].tar.gz
```
此时会解压出名为 bodhi-app-[发布编号]-[项目编号] 文件夹
```
cd bodhi-app-[发布编号]-[项目编号]
```
进入应用根目录
```
sudo vim repository/project/globalconf_database.json 
```
打开数据库配置文件，修改用户名、密码、地址等，以符合部署所在环境需要。
```
* ./bin/appctl.sh start 
```
启动项目
## 四、访问
* 浏览器输入**http://localhost:5301**